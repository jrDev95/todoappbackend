const Note = require('../models/Note');

const notesController = {};

notesController.getNotes = async (req,res) => {
  try{
    const notes = await Note.find();
    res.json(notes);
  }catch(err){
    res.json(err);
    return err
  }
}

notesController.getNote = async (req,res) => {
  try{
    const note = await Note.find({_id:req.params.id});
    res.json(note);
  }catch(err){
    res.json(err);
    return err;
  }
};


notesController.createNote = async (req,res) => {
  try{
    const {title, content, date, author} = req.body
    const newNote = new Note({
      title,
      content,
      date,
      author
    });
    res.json({message: "Created"})
    await newNote.save();
  }catch(err){
    res.json(err);
    return err
  }

}

notesController.updateNote = async (req,res) => {
  try {
    const {title, content, author} = req.body;
    await Note.findOneAndUpdate({_id:req.params.id}, {
      title,
      content,
      author
    });
    res.json({message: "Updated"});
  } catch (err) {
    res.json(err);
    return err;
  }
}

notesController.deleteNote = async (req,res) => {
  try {
    await Note.findByIdAndDelete(req.params.id)
    res.json({message: "Deleted"})

  } catch (e) {
    res.json(e);
    return e;
  }
}


module.exports = notesController;
