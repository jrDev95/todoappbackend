const express = require('express');
const cors = require('cors');
const app = express();

const users = require('./routes/users');
const notes = require('./routes/notes');

//Settings-----------------------

//Variable Globales
app.set('port', process.env.PORT);



//Middleware------------------------------------
app.use(cors());
app.use(express.json());


//Routes

app.use('/api/users', users);
app.use('/api/notes', notes);


module.exports = app
