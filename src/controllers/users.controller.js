const User = require('../models/User');

const usersController = {};

usersController.getUsers = async (req,res) => {
  try {
    const users = await User.find();
    res.json(users);
  } catch (e) {
    res.json(e);
    return e;
  }
}

usersController.getUser = async  (req,res) => {
  try {
    const user = await User.find({_id:req.params.id});
    res.json(user);
  } catch (e) {
    res.json(e);
    return e;
  }
}

usersController.createUser = async (req,res) => {
  try {
    const {username} = req.body;
    const newUser = new User({
      username,
    });
    await newUser.save();
    res.json({menssage:"User Created"});
  } catch (e) {
    res.json(e);
    return e;
  }


}

usersController.updateUser = async (req,res) => {
  try{
    const {username} = req.body;
    await User.findOneAndUpdate({_id:req.params.id},{username,});
    res.json({message:"User updated"});
  }catch(e){
    res.json(e);
    return e;
  }
}

usersController.deleteUser = async (req, res) => {
  try {
    await User.findByIdAndDelete(req.params.id);
    res.json({message:"User Deleted"});
  } catch (e) {
    res.json(e);
    return e;
  }
}

module.exports = usersController;
